# Welcome to the Mobile technical challenge of Plexure!

Please read through this readme ***carefully*** before you do anything!

**Before** you start to code, check if you have any questions (answered during normal office hours). After the answers, please send back your **planned goal** and **deadline** (a date, max 3 days).

## Requirements

- The solution needs to run on *at least* one device (can be a simulator as well) with at least one (iOS/Android) version.
- Provide the solution by sharing a link to a Git repository. We're interested in how you approach the problem, so please use small commits with clear commit messages. 
- Provide a readme markdown document about your finished solution and a description about its running environment. Any additional documentation is welcomed!
- Make sure that the solution compiles and runs!

## The App

The challenge is to create an app which has a basic feature set that you can then extend with additional features.

Create a simple store app where the user can browse a list of stores and he can add to and remove from a favorites list.

### Basic feature set

**UI:**

Setup 2 screens with a tab bar/Tab Layout. The first screen is the list of stores. Second screen is a favorite list. 

*List of stores:* Show the following store properties: Name, address, distance in KM , featureList. Introduce a Pull-to-refresh mechanism for this list with an activity indicator. Each store on the list has a favorite [Switch](https://developer.apple.com/ios/human-interface-guidelines/ui-controls/switches/) / [Toggle Button](https://developer.android.com/guide/topics/ui/controls/togglebutton). When it's switched on, the store is added to the favorites list. When it's switched off, the store is removed from the favorites list.

*List of favorites:* It shows all the stores which are favorited. Each item on this list has a delete button. When the delete button is tapped, that store is removed from the favorites list. This should be reflected on the stores list.

**Networking:**

Use the following network datasource to get the list of stores: https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db/data.json

**Data handling:**

Add a persistence layer for the app, which should be utilized by the favorites list. When the app terminates for any reason and is restarted, the favorites list needs to maintain a consistent state.

**Multithreading:**

Implement all the features so that the user interface is responsive and smooth at all times.

###Additional feature set

Now it's up to you to decide how to improve the product. Choose up to 4 features from the 7 below. Pick what you like. Express your strengths! 

* (**Data handling**) Introduce a filter feature. This feature postprocesses the received list and sorts the stores by their featureList property. It's would be a dialog with the list of feature List and when one feature got selected, the store list will be updated only showing the stores with the sleected feature. the dialog will have also All choice. 

* (**Data handling**) Introduce a filter feature. This feature postprocesses the received list and sorts the stores by their distance property deciding and assending. It's would be a button on top bar to filter  deciding/assending/none.

* (**Data handling**) Introduce a validation feature. This feature postprocesses the recieved list and grays out all far stores. A store is far when the the distance is more than 100 KM. Prevent users from adding far store to the favorites list.

* (**Multithreading**) Show the Address property of each store, but with a 2 seconds delay. Use the sleep() function (to simulate a heavy computing). The app should be still responsive all the time.

* (**Architecture**) Add a badge to the tab bar favorites list item. The badge should represent the number of vehicles that have been added to the favorites list since it was last viewed. When the user switches to the favorites list, the badge should disappear.

* (**Testing**) Introduce Unit tests

* (**Testing**) Introduce UI tests


*Pssst... [here's](https://github.com/raywenderlich/swift-style-guide) an amazing style guide for Swift* and [here's](https://github.com/raywenderlich/kotlin-style-guide) for Kotlin

---
#Have fun,  
Plexure  Mobile Team
